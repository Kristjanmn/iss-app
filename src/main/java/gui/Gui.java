package gui;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.shape.Line;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import model.Iss;

import java.io.IOException;

public class Gui extends Application {

    public Gui() {
        try {
            launch();   // Throws exception when called, because it for some reason triggers twice.
        } catch(IllegalStateException e) {
            //e.printStackTrace();
        }
    }


    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("IssScene.fxml"));
        //Group root = new Group();
        Scene scene = new Scene(root/*, 600, 400*/);
        // Use single CSS
        //scene.getStylesheets().add(getClass().getResource("gui.css").toExternalForm());
        // Use multiple CSS for multiple scenes
        String css = this.getClass().getResource("gui.css").toExternalForm();
        scene.getStylesheets().add(css);

        Image icon = new Image("file:src/main/resources/gui/icon.png");
        stage.getIcons().add(icon);
        stage.setTitle("ISS app");
        //stage.setWidth(800);
        //stage.setHeight(600);
        stage.setResizable(true);
        stage.setScene(scene);
        stage.show();

        //parent = root;
    }
}
