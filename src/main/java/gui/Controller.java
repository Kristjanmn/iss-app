package gui;

import com.jfoenix.utils.JFXUtilities;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.shape.Circle;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import model.Astronaut;
import model.Iss;

import java.io.IOException;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;

public class Controller {
    Iss iss = Iss.getIss();

    public void updateGui() {
        Platform.runLater(() -> {
            try {
                updateIss();
            } catch(Exception e) {
                e.printStackTrace();
            }
        });
    }

    private Stage stage;
    private Scene scene;
    private Parent root;
    public int sceneId = 0;                // Current scene id, probably temporary

    private String selectedAstronaut;

    /***** JavaFX variables *****/
    @FXML
    private ListView<String> astronautListView;
    @FXML
    private Label astronautName;
    @FXML
    private Label iss_latitude;
    @FXML
    private Label iss_longitude;
    @FXML
    private Label iss_altitude;
    @FXML
    private Label iss_velocity;
    @FXML
    private Label iss_astronautsNum;
    @FXML
    private Label iss_currentTime;

    @FXML
    public void initialize() throws IOException {
        iss.setController(this);
    }

    public void switchToIssScene(ActionEvent event) throws IOException {
        updateGui();
        sceneId = 0;
        iss.setController(this);
        root = FXMLLoader.load(getClass().getResource("IssScene.fxml"));
        stage = (Stage) ((Node)event.getSource()).getScene().getWindow();
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();

        String css = this.getClass().getResource("gui.css").toExternalForm();
        scene.getStylesheets().add(css);
        stage.fireEvent(new ActionEvent());
    }

    public void switchToAstronautsScene(ActionEvent event) throws IOException {
        updateGui();
        sceneId = 1;
        root = FXMLLoader.load(getClass().getResource("AstronautsScene.fxml"));
        stage = (Stage) ((Node)event.getSource()).getScene().getWindow();
        scene = new Scene(root);
        String css = this.getClass().getResource("gui.css").toExternalForm();
        scene.getStylesheets().add(css);
        stage.setScene(scene);
        stage.show();
        populateAstronautList();
    }

    public void populateAstronautList() {
        try {
            if(astronautListView == null) {
                // Get FXML item without ActionEvent
                astronautListView = (ListView<String>) root.lookup("#astronautListView");
            }
            List<Astronaut> astronautList = Iss.getAstronauts();
            // Clear existing list to avoid duplicates.
            astronautListView.getItems().clear();
            for(Astronaut astronaut : astronautList) {
                // Add all astronauts into the list.
                astronautListView.getItems().add(astronaut.getName());
            }

            // An astronaut has been selected from list.
            astronautListView.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
                @Override
                public void changed(ObservableValue<? extends String> observableValue, String s, String t1) {
                    selectedAstronaut = astronautListView.getSelectionModel().getSelectedItem();
                    if(astronautName == null) astronautName = (Label) root.lookup("#astronautName");
                    astronautName.setText(selectedAstronaut);
                }
            });
        } catch(IOException | InterruptedException e) {
            e.printStackTrace();
        }

    }

    private void updateIss() throws IOException {
        if(root == null) {
            root = FXMLLoader.load(getClass().getResource("IssScene.fxml"));
        }
        try {
            iss.setController(this);
            setIss_latitude(iss.getLatitude());
            setIss_longitude(iss.getLongitude());
            setIss_altitude(iss.getAltitude());
            setIss_velocity(iss.getVelocity(), iss.getUnits());
            setIss_currentTime();
            setIss_astronautsNum();
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    private void setIss_latitude(double value) {
        if(root == null) System.out.println("rot null");
        if(root == null) return;
        if (iss_latitude == null) iss_latitude = (Label) root.lookup("#iss_latitude");
        DecimalFormat format = new DecimalFormat("#.####");
        iss_latitude.setText(format.format(value));
    }

    private void setIss_longitude(double value) {
        if(root == null) return;
        if (iss_longitude == null) iss_longitude = (Label) root.lookup("#iss_longitude");
        DecimalFormat format = new DecimalFormat("#.####");
        iss_longitude.setText(format.format(value));
    }

    private void setIss_altitude(double value) {
        if(root == null) return;
        if (iss_altitude == null) iss_altitude = (Label) root.lookup("#iss_altitude");
        // Round altitude down to 2 decimal number
        DecimalFormat format = new DecimalFormat("#.##");
        iss_altitude.setText(format.format(value) + " km");
    }

    private void setIss_velocity(double value, String units) {
        if(root == null) return;
        String unit = "";      // km/h | m/h
        if (iss_velocity == null) iss_velocity = (Label) root.lookup("#iss_velocity");
        if(units.equalsIgnoreCase("kilometers")) unit = "km/h";
        if(units.equalsIgnoreCase("miles")) unit = "mph";
        // Round velocity(speed) down to 2 decimal number
        DecimalFormat format = new DecimalFormat("#.##");
        iss_velocity.setText(format.format(value) + " " + unit);
    }

    private void setIss_currentTime() {
        if(root == null) return;
        if (iss_currentTime == null) iss_currentTime = (Label) root.lookup("#iss_currentTime");
        DateTimeFormatter dateformat = DateTimeFormatter.ofPattern("d MMMM yyyy\nHH:mm:ss");
        this.iss_currentTime.setText(dateformat.format(LocalDateTime.now()));
    }

    private void setIss_astronautsNum() throws IOException, InterruptedException {
        if(root == null) return;
        if (iss_astronautsNum == null) iss_astronautsNum = (Label) root.lookup("#iss_astronautsNum");
        this.iss_astronautsNum.setText(Iss.getAstronauts().size()+"");
    }
}