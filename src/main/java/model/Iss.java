package model;

import com.mysql.cj.Session;
import gui.Controller;
import gui.Gui;
import javafx.scene.Parent;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.ArrayList;
import java.util.List;

public class Iss {

    private double latitude;
    private double longitude;
    private double altitude;
    private double velocity;
    private String units;           // kilometers/miles
    Controller controller;
    Gui gui;

    private Iss() {
        this.thread.start();
    }

    public static volatile Iss iss;
    public static Iss getIss() {
        if(iss == null) {
            iss = new Iss();
        }
        return iss;
    }

    public void setController(Controller controller) {
        this.controller = controller;
    }

    Runnable runnable = new Runnable() {
        @Override
        public void run() {
            while(true) {
                updateData();
                try {
                    if(controller != null) controller.updateGui();// System.out.println(controller.sceneId);
                    thread.sleep(500 * 1);     // refresh every 0.5 seconds
                } catch(InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    };
    Thread thread = new Thread(runnable);

    // Gui object has no further use after construction
    public void setGui(Gui gui) {
        this.gui = gui;
    }

    /**
     * Gets list of astronauts on ISS
     *
     * @return
     * @throws IOException
     * @throws InterruptedException
     */
    public static List<Astronaut> getAstronauts() throws IOException, InterruptedException {
        List<Astronaut> astronauts = new ArrayList<>();
        HttpClient client = HttpClient.newHttpClient();
        HttpRequest request = HttpRequest.newBuilder().uri(URI.create("http://api.open-notify.org/astros.json")).build();
        HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());
        JSONObject myObject = new JSONObject(response.body());
        //System.out.println(myObject); // Your json object

        JSONArray astronautArray = (JSONArray) myObject.get("people");
        for(int i = 0; i < astronautArray.length(); i++) {
            JSONObject astronautObj = (JSONObject) astronautArray.get(i);
            //System.out.println(astronautObj.get("name"));

            String name = astronautObj.get("name").toString();
            String craft = astronautObj.get("craft").toString();
            Astronaut astronaut = new Astronaut(name, craft);
            // Pretty useless check as it only gets people from ISS anyway.
            if(astronaut.getCraft().equalsIgnoreCase("ISS")) astronauts.add(astronaut);
        }
        return astronauts;
    }

    /**
     * Update local variables with json
     */
    private void updateData() {
        try {
            HttpClient client = HttpClient.newHttpClient();
            HttpRequest request = HttpRequest.newBuilder().uri(URI.create("https://api.wheretheiss.at/v1/satellites/25544.json")).build();
            HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());
            JSONObject myObject = new JSONObject(response.body());

            latitude = myObject.getDouble("latitude");
            longitude = myObject.getDouble("longitude");
            altitude = myObject.getDouble("altitude");
            velocity = myObject.getDouble("velocity");
            units = myObject.getString("units");                        // kilometers/miles

        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }

        /** Write data to database **/
        // TODO: ^^^^^^^^^^^^^^^^^

    }

    public double getLatitude() {
        return this.latitude;
    }

    public double getLongitude() {
        return this.longitude;
    }

    public double getAltitude() {
        return this.altitude;
    }

    public double getVelocity() {
        return this.velocity;
    }

    public String getUnits() {
        return this.units;
    }
}
