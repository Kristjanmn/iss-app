package model;

public class Astronaut {
    private final String name;
    private final String craft;

    public Astronaut(String name, String craft) {
        this.name = name;
        this.craft = craft;
    }

    public String getName() {
        return this.name;
    }

    public String getCraft() {
        return this.craft;
    }
}
