import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import gui.Controller;
import gui.Gui;
import model.Astronaut;
import model.Iss;
import org.json.JSONArray;
import org.json.JSONObject;

public class IssApp {

    public static void main(String[] args) throws IOException, InterruptedException {
        Iss iss = Iss.getIss();
        iss.setGui(new Gui());
        /*HttpClient client = HttpClient.newHttpClient();
        HttpRequest request2 = HttpRequest.newBuilder().uri(URI.create("http://api.open-notify.org/iss-now.json")).build();
        HttpResponse<String> response2 = client.send(request2, HttpResponse.BodyHandlers.ofString());
        JSONObject myObject2 = new JSONObject(response2.body());
        System.out.println(myObject2); // Your json object

        System.out.println( myObject2.get("iss_position"));

        JSONObject issPos = (JSONObject) myObject2.get("iss_position");
        System.out.println(issPos.get("latitude"));
        System.out.println(issPos.get("longitude"));

        double lat = Double.parseDouble((String) issPos.get("latitude"));
        System.out.println(lat);

        List<Astronaut> astronauts = Iss.getAstronauts();
        for(Astronaut astronaut : astronauts) {
            System.out.println(astronaut.getName() + " " + astronaut.getCraft());
        }*/

        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                Scanner scanner = new Scanner(System.in);
                while(true) {
                    // TODO: terminal commands and stuff
                }
            }
        };
        Thread thread = new Thread(runnable);
    }
}
